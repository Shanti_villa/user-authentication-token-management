const app = require('../server');
const db = require('../config/db');
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');

const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;
describe('POST /register', () => {

  const sampleUser = { id: 1, username: 'sampleUser' };

  it('should register with status 200', async () => {
    const candidateData = {
        email:'shanti.v@gmail.com',
        password:'shanti',
        first_name: 'shanti',
        last_name: 'villa'
      };
    const sampleUser = { id: 1, username: 'sampleUser' };
    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 

    //  request with authenticated token
    const response = await axios.post(BASE_URL+'/v1/register', candidateData);
    expect(response.status).to.equal(200);
    
  });


  it('should return 500 for server/database errors', async () => {
    const candidateData = {
        email:'shanti.v@gmail.com',
        first_name: 'shanti',
        last_name: 'villa',
      };

    try {
  
      let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
      await axios.post(BASE_URL+'/v1/register', candidateData);
    } catch (error) {
    
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(500);
      expect(error.response.data.message).to.equal('Internal Server Error');
    }
  });
});
