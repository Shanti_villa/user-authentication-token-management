const app = require('../server'); 
const db = require('../config/db'); 
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');

const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;

describe('GET /candidates', () => {
  
  const sampleUser = { id: 1, username: 'sampleUser' };

  let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 

  it('should return candidates with status 200', async () => {
    // request with the authentication token
    const response = await axios.get(BASE_URL+`/v1/candidates/`, {
      headers: { Authorization: `Bearer ${token}` },
    });

    expect(response.status).to.equal(200);
  });

  it('should return 401 for unauthenticated request', async () => {
    try {
      // request with unauthenticated token
      await axios.get(BASE_URL+`/v1/candidates/`);
    } catch (error) {
     
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(401);
      expect(error.response.data.error).to.equal('Access token is invalid');
    }
  });

  it('should return 500 for server/database errors', async () => {
   
    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 

    try {
      // Make the request with the authentication token in the headers
      await axios.get(BASE_URL+'/v1/candidates', {
        headers: { Authorization: `Bearer ${token}` },
      });
    } catch (error) {
     
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(500);
      expect(error.response.data.message).to.equal('Internal Server Error');
    }
  });
});
