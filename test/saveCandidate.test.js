const app = require('../server'); 
const db = require('../config/db'); 
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');

// Use chai's expect function for assertions
const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;

describe('POST /candidates', () => {

  const sampleUser = { id: 1, username: 'sampleUser' };


  it('should create a new candidate with status 200', async () => {
    const candidateData = {
        first_name: 'satyaShanti',
        last_name: 'villaa',
        age: 26,
        owner_id : '12c0080-1asd-45d7-bccb-37a1c6dca0d1',
        department: 'IT',
        min_salary_expectation: 50000,
        max_salary_expectation: 80000,
        currency_id: 1,
        address_id: 23
      };
  
    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
    // request with authenticated token
    const response = await axios.post(BASE_URL+'/v1/candidates', candidateData, {
      headers: { Authorization: `Bearer ${token}` },
    });

    // Assert that the response status is 200
    expect(response.status).to.equal(200);
    // Assert that the response contains the expected message
    expect(response.data.message).to.equal('Candidate saved successfully');
    
  });

  it('should return 401 for unauthenticated request', async () => {
    const candidateData = {
      first_name: 'satyaShanti',
      last_name: 'villaa',
      age: 26,
      owner_id : '123vc0080-1asd-45d7-bccb-37a1c6dca0d1',
      department: 'IT',
      min_salary_expectation: 50000,
      max_salary_expectation: 80000,
      currency_id: 1,
      address_id: 23
    };

    try {
      // request with unauthenticated token
      const response = await axios.post(BASE_URL+'/v1/candidates', candidateData, {
      headers: { Authorization: `Bearer sdjfhasjkdhflkashdf` },
    });
    } catch (error) {
     
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(401);
      expect(error.response.data.message).to.equal('Access token is invalid');
    }
  });

  it('should return 500 for server/database errors', async () => {
    const candidateData = {
      first_name: 'satyaShanti',
      last_name: 'villaa',
      owner_id : '123vc0080-1asd-45d7-bccb-37a1c6dca0d1',
      department: 'IT',
      min_salary_expectation: 50000,
      max_salary_expectation: 80000,
      currency_id: 1,
      address_id: 23
    };

   
    try {
    
      let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
      await axios.post(BASE_URL + '/v1/candidates', candidateData, {
        headers: { Authorization: `Bearer ${token}` },
      });
    } catch (error) {
     
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(500);
      expect(error.response.data.error).to.equal('Internal Server Error');
    }
  });
});
