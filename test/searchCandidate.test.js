const app = require('../server'); 
const db = require('../config/db');
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');

const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;

describe('POST /candidates/search', () => {
 
  const sampleUser = { id: 1, username: 'sampleUser' };

  it('should return search results with status 200', async () => {
    const searchQuery = 'villa'; // Search query

    const requestBody = {
      query: searchQuery,
      page: 1,
    };
    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' });

    //  request with authenticated token
    const response = await axios.post(BASE_URL + '/v1/candidates/search', requestBody, {
      headers: { Authorization: `Bearer ${token}` },
    });

    expect(response.status).to.equal(200);
  });

  it('should return 500 for server/database errors', async () => {
    const searchQuery = 'John'; 
    const requestBody = {
      query: searchQuery,
      page: 1,
    };

    try {
        let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
      //  request with authenticated token
      await axios.post(BASE_URL + '/v1/candidates/search', requestBody, {
        headers: { Authorization: `Bearer ${token}` },
      });
    } catch (error) {
    
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(500);
      expect(error.response.data.message).to.equal('Internal Server Error');
    }
  });
});
