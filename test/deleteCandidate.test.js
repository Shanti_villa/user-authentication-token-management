const app = require('../server'); 
const db = require('../config/db'); 
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');

const expect = chai.expect;

const BASE_URL = process.env.BASE_URL;

describe('DELETE /candidates/:id', () => {
 
  const sampleUser = { id: 1, username: 'sampleUser' };


  it('should delete the candidate with status 200', async () => {
    const candidateId = '1';
    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' });

     // request with authenticated token
    const response = await axios.delete(BASE_URL+`/v1/candidates/${candidateId}`, {
      headers: { Authorization: `Bearer ${token}` },
    });

    expect(response.status).to.equal(200);
    expect(response.data.message).to.equal('Candidate deleted successfully');
  });

  it('should return 404 for trying to delete a non-existing candidate', async () => {
    const candidateId = 'non_existing_id';
    try {
     // request with authenticated token
      let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
      await axios.delete(BASE_URL+`/v1/candidates/${candidateId}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    } catch (error) {
     
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(404);
      expect(error.response.data.message).to.equal('Candidate not found');
    }
  });

  it('should return 401 for unauthenticated request', async () => {
    const candidateId = '1';

    try {
     // request with unauthenticated token
      await axios.delete(BASE_URL+`/v1/candidates/${candidateId}`);
    } catch (error) {
      
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(401);
      expect(error.response.data.message).to.equal('Access token is invalid');
    }
  });

  it('should return 500 for server/database errors', async () => {
    const candidateId = '1';
    try {
        let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 

      // request with authenticated token
      await axios.delete(BASE_URL+`/v1/candidates/${candidateId}`, {
        headers: { Authorization: `Bearer ${token}` },
      });
    } catch (error) {
  
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(500);
      expect(error.response.data.message).to.equal('Internal Server Error');
    }
  });
});
