const app = require('../server'); 
const db = require('../config/db'); 
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const bcrypt = require('bcrypt');

const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;

describe('POST /login', () => {
 
  const sampleUser = {
    id: 1,
    email: 'shanti.v@gmail.com',
    password: bcrypt.hashSync('shanti', 10),
  };

  it('should return access token and refresh token with status 200 for valid credentials', async () => {
    const email = 'shanti.v@gmail.com';
    const password = 'shanti';


    //  request with valid credentials
    const response = await axios.post(BASE_URL+'/v1/login', { email, password });

    expect(response.status).to.equal(200);
    expect(response.data.message).to.equal('User LoggedIn Successfully');
  });

  it('should return 500 for server errors', async () => {
    const email = 'test@example.com';
    const password = 'testpassword';

    //request with valid credentials
    try {
      await axios.post(BASE_URL+'/v1/login', { email, password });
    } catch (error) {
    
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(500);
      expect(response.data.message).to.equal('Internal Server Error');
    }
  });
});
