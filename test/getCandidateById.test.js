const app = require('../server');
const db = require('../config/db');
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');

const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;
describe('GET /candidates/:id', () => {
 
  it('should return candidate details with status 200', async () => {
    const candidateId = 1;
  
    // Create a mock authentication token
    const sampleUser = { id: 1, username: 'sampleUser' };
    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 

    const response = await axios.get(BASE_URL+`/v1/candidates/${candidateId}`, {
      headers: { Authorization: `Bearer ${token}` }, 
    });
  
    expect(response.status).to.equal(200);
  });
  it('should return candidate details with status 401 for unauthenticated request', async () => {
    const candidateId = 1;

    // Mock the db.query function to return an empty array for the given candidateId
    const dbQueryStub = sinon.stub(db, 'query').resolves([]);

    try {
        await axios.get(BASE_URL+`candidates/${candidateId}`, {
            headers: { Authorization: `Bearer hajlsdgfjalhgyuewgrjhasgdylgsadjfhgsady` },
          });
    } catch (error) {
        if (error.isAxiosError) {
           
            expect(error.response).to.exist;
            expect(error.response.status).to.equal(401);
            expect(error.response.data.error).to.equal('Access token is invalid');
          } else {
            throw error;
          }
    }

  });

  it('should return 404 for candidate not found', async () => {
    const candidateId = "kgjsoighe3254";

    try {
        const sampleUser = { id: 1, username: 'sampleUser' };
        let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
    
        const response = await axios.get(BASE_URL+`/v1/candidates/${candidateId}`, {
          headers: { Authorization: `Bearer ${token}` }, 
        });
    } catch (error) {
  
      expect(error.response.status).to.equal(404);
      expect(error.response.data.error).to.equal('Candidate not found');

    }

  });
});
