const app = require('../server'); 
const db = require('../config/db');
const axios = require('axios');
const sinon = require('sinon');
const chai = require('chai');
const jwt = require('jsonwebtoken');
const fs = require('fs');
const privateKey = fs.readFileSync('private_key.pem');
const sampleUser = { id: 1, username: 'sampleUser' };

const expect = chai.expect;
const BASE_URL = process.env.BASE_URL;

describe('POST /token/refresh', () => {

  it('should return 200 and a new access token when providing a valid refresh token', async () => {
   
    const sampleRefreshToken = 'eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjM4MGMwMDgwLTFhMDAtNDVkNy1iY2NiLTM3YTFjNmRjYTBkMSIsImlhdCI6MTY5MDIzNDQzMSwiZXhwIjoxNjkyODI2NDMxfQ.XyhMNs1k_Ejeaw0lb9R_bhWC8dTI5uIJwWPakurZXDUQ0XVR6Vf1Em_Qb_ukMhte3CCt1f1uqOZj57MGVtnjTPxavwCkJeuyREfhz7GNAPqiyoF4Suv8niBPM8NC2Sh6edFRfTOsfb_tqxhlt4y4gY59AATf1U4C-GZGxyXYHitSj4w7I4Yxg83Xr3tq54DrbunkHG2j6f1OYX2a0KQDfo406pDrT3cpRZUUER3rWnvSuCvGwcgULpgBS11VqtbVsHHD54_coVorXiXvfaWNSdSUbKe8myl1DxelrMZpOrMhS-kEPJOWgKgtIAsle_4vEF9nwzGQcnhkh8jGpRfb_g';

    let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' }); 
    const response = await axios.post(BASE_URL + `/v1/token/refresh`, { refreshToken: sampleRefreshToken }, {
    headers: { Authorization: `Bearer ${token}`,
    Cookie: `refreshToken=${sampleRefreshToken}` },
    });

    expect(response.status).to.equal(200);
    expect(response.data.accessToken).to.exist;
    expect(response.data.message).to.equal('Successfully generated the new access token');
  });

  it('should return 400 if refresh token is not provided', async () => {
   
    try {
        let token = jwt.sign(sampleUser, privateKey, { algorithm: 'RS256', expiresIn: '1h' });
        const response = await axios.post(BASE_URL + `/v1/token/refresh`, null, {
        headers: { Authorization: `Bearer ${token}` },
        });
    } catch (error) {
      
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(400);
      expect(error.response.data.message).to.equal('Refresh token is Missing.');
    }
  });

  it('should return 401 for invalid refresh tokens', async () => {

  
    try {
      await axios.post(BASE_URL + '/v1/token/refresh', { refreshToken: 'invalid_refresh_token' }, {
        headers: { Authorization: 'Bearer sample_access_token' }, // Include the bearer token in the header
      });
    } catch (error) {
      
      expect(error.response).to.exist;
      expect(error.response.status).to.equal(401);
      expect(error.response.data.message).to.equal('Invalid refresh token.');
    }
  });


});
