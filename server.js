const express = require('express');
const bodyParser = require('body-parser');
const userRoutes = require('../app/routes/userRoutes');
// const swaggerSetup = require('./swagger');
const swaggerUi = require('swagger-ui-express');
const SwaggerExpressMiddleware = require('swagger-express-middleware');
const swaggerDocument = require('./swagger.json');
const cors = require('cors');
const cookieParser = require('cookie-parser');
const dotenv = require('dotenv');

dotenv.config();
const app = express();

app.use(express.json());
const corsOptions = {
    origin: process.env.BASE_URL, // Replace with frontend URL
    credentials: true, // Allow cookies to be sent
};
app.use(cors(corsOptions));
// Parse URL-encoded requests
app.use(express.urlencoded({ extended: true }));
app.use(cookieParser());
app.use(bodyParser.json());

app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));

app.use('/v1', userRoutes);


const port = process.env.PORT || 3000;

app.listen(port, () => {
  console.log(`Server is listening on port ${port}`);
});




