const jwt = require('jsonwebtoken');
const fs = require('fs');
const dotenv = require('dotenv');
dotenv.config();

const privateKey = fs.readFileSync('private_key.pem');
const publicKey = fs.readFileSync('public_key.pem');


// Function Name: generateAccessToken
// Purpose: It will create a access token for the user

const generateAccessToken = (payload) => {
  return jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn: '50d' });
};

// Function Name: generateRefreshToken
// Purpose: It will generate a refresh token for the user

const generateRefreshToken = (payload) => {
 
  const refreshTokenExpiresIn = '10d';

  const refreshToken = jwt.sign(payload, privateKey, { algorithm: 'RS256', expiresIn: refreshTokenExpiresIn });

  return refreshToken;
};

// Function Name: verifyAccessToken
// Purpose: It will verify whether the user is authorized user or not using the bearer token

const verifyAccessToken = (req, res, next) => {
  let token = req.headers['authorization'];
  console.log("token "+ token)
  if(token){
    token = token.replace('Bearer ', '');
  }
  if (!token) {
    return res.status(401).json({ error: 'Unauthorized Request' });
  }
  
  jwt.verify(token, publicKey, { algorithms: ['RS256'] }, (err, user) => {
    if (err) return res.status(401).json({ error: 'Unauthorized Request' });;
    req.user = user;
    next();
  });
};

// Function Name: verifyRefreshToken
// Purpose: It will verify whether the refresh token is valid or not.


const verifyRefreshToken = (refreshToken) => {
  console.log("enteredmid")
  if (!refreshToken) {
    return res.status(401).json({ message: 'Refresh token is missing' });
  }
  console.log("above")
  try {
    // Verify and decode the refresh token
    console.log(refreshToken);
    console.log(privateKey);
    const decodedToken = jwt.verify(refreshToken, privateKey, { algorithms: ['RS256'] });
    return decodedToken;
  } catch (error) {
    throw new Error('Invalid refresh token.');
  }
};




module.exports = { generateAccessToken, generateRefreshToken, verifyAccessToken, verifyRefreshToken };
