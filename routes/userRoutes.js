const express = require('express');
const router = express.Router();
const tokenController = require('../controllers/tokenRefreshController')
const userController = require('../controllers/userController');
const candidatesController = require('../controllers/candidatesController');


router.post('/token/refresh', tokenController.refreshToken);
router.post('/register', userController.registerUser);
router.post('/logout', userController.logout);
router.post('/login', userController.login);
router.post('/candidates', candidatesController.saveCandidate);
router.get('/candidates/:id', candidatesController.getCandidate);
router.get('/candidates', candidatesController.getAllCandidates);
router.post('/candidates/search', candidatesController.searchCandidates);
router.delete('/candidates/:id', candidatesController.deleteCandidate);
router.post('/candidates/:id', candidatesController.updateCandidate);

module.exports = router;
