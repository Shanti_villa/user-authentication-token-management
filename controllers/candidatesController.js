const db = require('../config/db');
const { v4: uuidv4 } = require('uuid');
const { verifyAccessToken } = require('../middleware/authMiddleware'); // Import authentication middleware

exports.saveCandidate = async (req, res) => {

    verifyAccessToken(req, res, (err) => {
        if (err) {
        // Handle any errors from the middleware
        return res.status(401).json({ message: 'Access token is invalid' });
        }
    })
    console.log(req.user)
    try {
        let owner_id = req.body.owner_id ? req.body.owner_id : req.user.id
       
        // Extract candidate data from the request body
      
        const { first_name, last_name, age, department, min_salary_expectation, max_salary_expectation, currency_id, address_id } = req.body;
  
        const candidateData = {
            id: uuidv4(),
            first_name,
            last_name,
            owner_id,
            age,
            department,
            min_salary_expectation,
            max_salary_expectation,
            currency_id,
            address_id,
          };
      
          const query = 'INSERT INTO candidate_t SET ?';
          const newCandidate = await db.query(query, candidateData)
          res.status(200).json({ message: 'Candidate saved successfully', candidateId: newCandidate.id });
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
      }
}

exports.getCandidate = async (req, res) => {

    verifyAccessToken(req, res, (err) => {
        if (err) {
        // Handle any errors from the middleware
        return res.status(401).json({ message: 'Access token is invalid' });
        }
    })

    try {
        const candidateId = req.params.id;

        const query = 'SELECT * FROM candidate_t WHERE id = ?';
        const candidateData = (await db.query(query, [candidateId]))[0].map((row) => row);
    
        if (candidateData.length === 0) {
            return res.status(404).json({ message: 'Candidate not found' });
        }
  
        const candidate = candidateData[0];
   
        res.status(200).json({"candidate":candidate});

    } catch (error) {
        console.error(error);
        res.status(500).json({ message : 'Internal Server Error' });
    }
  
}

exports.getAllCandidates = async (req, res) => {

    const page = parseInt(req.query.page) || 1;
    const pageSize = parseInt(req.query.pageSize) || 10;
  
    try {
      // Calculate the offset based on pagination parameters
      const offset = (page - 1) * pageSize;
  
      // Query candidates with pagination
      const query = 'SELECT * FROM candidate_t LIMIT ? OFFSET ?';
      const candidates = (await db.query(query, [pageSize, offset]))[0].map((row) => row);

      res.status(200).json({"candidates" :candidates});
    } catch (error) {

      console.error(error);
      res.status(500).json({ message: 'Internal server error' });
    }
}

exports.searchCandidates = async (req, res) => { 
    const { name, page } = req.body; // Criteria for candidate search
    const pageSize = 10;

    try {
        // Calculate the offset based on pagination parameters
        const offset = (page - 1) * pageSize;

        const query = 'SELECT * FROM candidate_t WHERE first_name LIKE ? OR last_name LIKE ? LIMIT ? OFFSET ?';
        const candidates = (await db.query(query, [`%${name}%`, `%${name}%`, pageSize, offset]))[0].map((row) => row);
    
        res.status(200).json({"candidates": candidates});
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
      }
}

exports.deleteCandidate = async (req, res) => {

    verifyAccessToken(req, res, (err) => {
        if (err) {
        // Handle any errors from the middleware
        return res.status(401).json({ message: 'Access token is invalid' });
        }
    })

    const candidateId = req.params.id;

    try {
      
        const query = 'SELECT * FROM candidate_t WHERE id = ?';
        const candidates = await db.query(query, [candidateId]);
    
        if (!candidates || candidates.length === 0) {
          return res.status(404).json({ message: 'Candidate not found' });
        }
    
       // Delete the candidate from the database
       const deleteQuery = 'DELETE FROM candidate_t WHERE id = ?';
       await db.query(deleteQuery, [candidateId]);
    
       res.json({ message: 'Candidate deleted successfully' });

    } catch (error) {

        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}

exports.updateCandidate = async (req, res) => {

    verifyAccessToken(req, res, (err) => {
        if (err) {
        // Handle any errors from the middleware
        return res.status(401).json({ message: 'Access token is invalid' });
        }
    })

    const candidateId = req.params.id;

    const { first_name, last_name, age, department, min_salary_expectation, max_salary_expectation, currency_id, address_id } = req.body;

    try {
       
        const query = 'SELECT * FROM candidate_t WHERE id = ?';
        const candidate = await db.query(query, [candidateId]);

    
        if (!candidate || candidate.length === 0) {
          return res.status(404).json({ message: 'Candidate not found' });
        }
    
        const updateQuery =
        'UPDATE candidate_t SET first_name = ?, last_name = ?, age = ?, department = ?, min_salary_expectation = ?, max_salary_expectation = ?, currency_id = ?, address_id = ? WHERE id = ?';
  
        await db.query(updateQuery, [
            first_name,
            last_name,
            age,
            department,
            min_salary_expectation,
            max_salary_expectation,
            currency_id,
            address_id,
            candidateId,
        ]);
    
        res.status(200).json({ message: "Successfully updated the candidate data"});
    } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
    }
}



