const User = require('../models/UserModel');
const db = require('../config/db');
const bcrypt = require('bcryptjs');
const { v4: uuidv4 } = require('uuid');
const { verifyAccessToken, verifyRefreshToken, generateAccessToken, generateRefreshToken } = require('../middleware/authMiddleware'); // Import authentication middleware


exports.registerUser = async (req, res) => {
  console.log("entered")
  try {
    const { email, password, first_name, last_name } = req.body;
    // Check if the user already exists
    const userQuery = 'SELECT * FROM user_t WHERE email = ?';
    const isExistedUser = (await db.query(userQuery, [email]))[0].map((row) => row);
   
    if(isExistedUser && isExistedUser[0]?.email) {
        return res.status(400).json({ error: 'user_exists', error_description: 'User with the same email already exists.' });
    }
    // create a new user
    const hashedPassword = await bcrypt.hash(password, 10);
   
    const userData = {
        id: uuidv4(),
        email,
        password: hashedPassword,
        first_name,
        last_name,
    };

    const query = 'INSERT INTO user_t SET ?';
    const [user] = await db.query(query, userData)
  
    res.status(200).json({ message: 'User registered successfully' });
  } catch (error) {

    console.error(error);
    res.status(500).json({ message : 'Internal server error' });
  }
};


exports.logout = async (req, res, next) => {
  
     // Call the verifyAccessToken middleware
    verifyAccessToken(req, res, (err) => {
        if (err) {
        // Handle any errors from the middleware
        return res.status(401).json({ message: 'Access token is invalid' });
        }
    })
   
    let refresh_token  = req.body.refreshToken ?  req.body.refreshToken : req.cookies.refreshToken;
  
    if(!refresh_token) {
        return res.status(400).json({ error: 'Refresh token is Missing.' });
    }

    try {
     
        const decodedToken = verifyRefreshToken(refresh_token);
       
        res.clearCookie('refreshToken');
    
        res.status(200).json({ message: 'Refresh token invalidated successfully' });

      } catch (error) {

        console.error(error);

        res.status(500).json({ message : 'An error occurred while invalidating the refresh token' });
      }


}


exports.login = async (req, res, next) => {
    try {
        const { email, password } = req.body;
    
        let userDetails = (await db.query('SELECT * FROM user_t WHERE email = ? ', [email]))[0].map((row) => row);

        if (!Array.isArray(userDetails)) {
            userDetails = [userDetails];
        }
      
        const isPasswordValid = await bcrypt.compare(password, userDetails[0]?.password);
      
        if (!isPasswordValid) {
          return res.status(400).json({ message: 'Invalid credentials' });
        }
      
         // Generate an access token
        const accessToken = generateAccessToken({ id: userDetails[0].id});
        // Generate a refresh token 
        const refreshToken = generateRefreshToken({ id: userDetails[0].id});

        res.cookie('refreshToken', String(refreshToken), { httpOnly: true });
        res.status(200).json({'accessToken': accessToken, 'refreshToken': refreshToken, "message":"User LoggedIn Successfully" });
      
      } catch (error) {
        console.error(error);
        res.status(500).json({ message: 'Internal server error' });
      }
}


