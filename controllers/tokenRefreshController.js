const jwt = require('jsonwebtoken');
// const { generateAccessToken,verifyRefreshToken } = require('../middleware/authMiddleware');
const { verifyRefreshToken, generateAccessToken } = require('../middleware/authMiddleware'); // Import authentication middleware

exports.refreshToken = async (req, res) => {

    let refresh_token = req.body.refreshToken ?  req.body.refreshToken : req.cookies.refreshToken;

    if(!refresh_token) {
        return res.status(400).json({ error: 'Refresh token is Missing.' });
    }
  
    try {
       
        const decodedToken = verifyRefreshToken(refresh_token);
        //If refresh token is verified successfully then we will get the id by decoding refresh token
        const userId = decodedToken.id;
    
        const newAccessToken = generateAccessToken({ id: userId });
    
        res.status(200).json({ accessToken: newAccessToken, token_type: 'Bearer', "message":"Successfully generated the new access token" });
     
    } catch (error) {

        res.status(401).json({ message: 'Invalid refresh token.' });
      }

};

